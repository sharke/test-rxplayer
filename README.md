# exo RxPlayer

cette exo se base sur un starter que j'avais fait moi même ( younes.C ) basé sur vueJs 2.6

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve --port=7575
```

### Compiles and minifies for production

```
yarn build
```

### Run your unit tests

```
yarn test:unit
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### lancer le server local des données test ( todo )

```
json-server --watch src/assets/mocks/todos.json --port 5151
```

#####

https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/
BigBuckBunny.mp4
